import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ExoplanetGridConfig} from './ag-grid/services/exoplanet-grid-config.service';
import {ExoplanetService} from './ag-grid/services/exoplanet.service';
import {StatsService} from './ag-grid/services/stats.service';
import {BarChartDirection} from './charts/model/bar-chart/bar-chart-direction';
import {BarChartStyle} from './charts/model/bar-chart/bar-chart-style';
import {MultiSeries} from './charts/model/multi-series';
import {SingleSeries} from './charts/model/single-series';
import {standardOperations} from './charts/model/chart-constants';
import {CHART_COLUMNS, CHART_OPERATIONS, DEFAULT_GROUP} from './charts/model/chart-constants';
import {ChartType} from './charts/model/chart-type';
import {exoplanetColumns} from './model/exoplanet-columns';
import {SplitService} from './services/spllit.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {provide: StatsService, useClass: ExoplanetService},
        {provide: CHART_OPERATIONS, useValue: standardOperations},
        {provide: CHART_COLUMNS, useValue: exoplanetColumns},
        {provide: DEFAULT_GROUP, useValue: 'discoveryMethod'}
    ]
})
export class AppComponent implements OnInit {

    ChartType = ChartType;

    private static readonly splitStorageKey = 'splitSizes';

    mode: string;
    splitSizes: number[];
    title = 'greg-ag-grid-filters-ui';

    chartConfig: any;
    chartDirection = BarChartDirection.VERTICAL;
    chartStyle = BarChartStyle.NORMALIZED;
    data: SingleSeries | MultiSeries;

    constructor(public readonly config: ExoplanetGridConfig,
                public readonly splitService: SplitService) {
    }

    ngOnInit() {
        this.splitSizes = this.splitService.getSplit(AppComponent.splitStorageKey);
    }

    saveSplit(sizes: number[]) {
        this.splitService.setSplit(AppComponent.splitStorageKey, sizes);
    }

}
