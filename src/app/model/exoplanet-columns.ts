import {DataColumn, DataColumnType, DataColumnUsage} from '../charts/model/data-column';

export const exoplanetColumns = [
        new DataColumn('exoplanetId', 'Instances', DataColumnType.ID, DataColumnUsage.AGGREGATE),
        new DataColumn('name', 'Planet Name', DataColumnType.STRING, DataColumnUsage.GROUP),
        new DataColumn('discoveryMethod', 'Discovery Method', DataColumnType.STRING, DataColumnUsage.GROUP),
        new DataColumn('facility', 'Facility', DataColumnType.STRING, DataColumnUsage.GROUP),
        new DataColumn('telescope', 'Telescope', DataColumnType.STRING, DataColumnUsage.GROUP),
        new DataColumn('instrument', 'Instrument', DataColumnType.STRING, DataColumnUsage.GROUP),
        new DataColumn('starName', 'Star Name', DataColumnType.STRING, DataColumnUsage.GROUP),
        new DataColumn('planetCount', 'Planet Count', DataColumnType.NUMBER, DataColumnUsage.ALL),
        new DataColumn('lastUpdated:year', 'Last Updated Year', DataColumnType.DATE, DataColumnUsage.GROUP),
        new DataColumn('lastUpdated:month', 'Last Updated month', DataColumnType.DATE, DataColumnUsage.GROUP)
    ]
