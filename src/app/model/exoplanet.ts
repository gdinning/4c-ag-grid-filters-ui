export interface Exoplanet {
    id: number;
    starName: string;
    planetLetter: string;
    name: string;
    discoveryMethod: string;
    planetCount: number;
    lastUpdated: Date;
    facility: string;
    telescope: string;
    instrument: string;
    link: string;
}
