export interface FilterValue {
    type: string;
    value: string | number | Date;
    toValue?: string | number | Date;
}
