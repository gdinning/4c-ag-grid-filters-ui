import {Predicate} from './predicate';
import {FilterValue} from './filter-value';

export interface SimpleFilter {
    predicate: string;
    filter: FilterValue;
}
