import {Stat} from './stat';

export interface NamedSeries {

    name: string;
    series: Stat[];

}
