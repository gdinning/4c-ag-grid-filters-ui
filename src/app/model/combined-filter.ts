import {SimpleFilter} from './simple-filter';

export class CombinedFilter {
    predicate: string;
    condition1: SimpleFilter;
    condition2: SimpleFilter;
}
