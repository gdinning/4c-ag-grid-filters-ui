export interface Employee {
  name: string;
  sickDays: number;
}
