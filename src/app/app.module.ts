import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AgGridViewModule} from './ag-grid/ag-grid-view.module';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ChartsModule} from './charts/charts.module';
import {ToggleButtonModule} from './toggle-button/toggle-button.module';
import {AngularSplitModule} from 'angular-split';

@NgModule({
    declarations: [AppComponent],
    imports: [
        AngularSplitModule.forRoot(),
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AgGridViewModule,
        FontAwesomeModule,
        ChartsModule,
        ToggleButtonModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
