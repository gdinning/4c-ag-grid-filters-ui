import {Module} from '@ag-grid-community/all-modules';
import {AllEnterpriseModules} from '@ag-grid-enterprise/all-modules';
import {Component, Input} from '@angular/core';
import {FilterChangedEvent} from 'ag-grid-community';
import {FilterService} from 'src/app/services/filter.service';
import {GridConfig} from '../../model/grid-config';

@Component({
    selector: 'app-ag-grid-view',
    templateUrl: './ag-grid-view.component.html',
    styleUrls: ['./ag-grid-view.component.scss']
})
export class AgGridViewComponent {

    @Input() config: GridConfig<any>;
    @Input() showTotals = true;

    modules: Module[] = AllEnterpriseModules;

    constructor(private readonly filterService: FilterService) {
    }

    filtersChanged(event: FilterChangedEvent) {
        this.filterService.setFilters(event.api.getFilterModel());
    }

}
