import {ColDef, GridApi, GridOptions, GridReadyEvent, IServerSideDatasource} from '@ag-grid-community/all-modules';
import {EventEmitter} from '@angular/core';
import {SelectFilterComponent} from '../modules/select-value-filter/components/select-filter/select-filter.component';
// tslint:disable-next-line: max-line-length
import {SelectFloatingFilterComponent} from '../modules/select-value-filter/components/select-floating-filter/select-floating-filter.component';

export class GridConfig<T> {
    api: GridApi;
    columnAPi: any;

    columns: ColDef[];
    rowData: T[];

    filterChanged = new EventEmitter<any>();

    constructor(private readonly datasource?: IServerSideDatasource) {
    }

    getOptions(): GridOptions {
        return {
            suppressCellSelection: true,
            enableBrowserTooltips: true,
            enableRangeSelection: true,
            defaultColDef: this.getColumnDefDefaults(),
            rowModelType: this.datasource != null ? 'serverSide' : 'clientSide',
            floatingFilter: true,
            frameworkComponents: {
                selectFilter: SelectFilterComponent,
                selectFloatingFilter: SelectFloatingFilterComponent
            }
        };
    }

    getColumnDefDefaults(): ColDef {
        return {
            sortable: true,
            filter: true,
            resizable: true,
            filterParams: {
                resetButton: true
            }
        };
    }

    onGridReady(event: GridReadyEvent) {
        this.api = event.api;
        this.columnAPi = event.columnApi;

        if (this.datasource != null) {
            this.api.setServerSideDatasource(this.datasource);
        }
    }
}
