import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Employee} from '../../model/employee';
import {Observable} from 'rxjs';
import {SimpleFilter} from 'src/app/model/simple-filter';
import {CombinedFilter} from 'src/app/model/combined-filter';
import {FilterFactory} from './filter-factory.service';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {
    constructor(
        private readonly http: HttpClient,
        private readonly filterFactory: FilterFactory
    ) {}

    getEmployees(filters: any): Observable<Employee[]> {
        console.log('Filters', this.filterFactory.buildFilters(filters));
        return this.http.get<Employee[]>('http://localhost:8080/employees', {
            params: this.filterFactory.buildParams(filters)
        });
    }
}
