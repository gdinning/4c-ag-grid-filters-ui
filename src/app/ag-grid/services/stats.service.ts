import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {MultiSeries} from 'src/app/charts/model/multi-series';
import {SingleSeries} from 'src/app/charts/model/single-series';
import {Stat} from 'src/app/model/stat';
import {FilterFactory} from './filter-factory.service';
import {NamedSeries} from 'src/app/charts/model/named-series';

export abstract class StatsService {

    protected abstract getBaseUrl(): string;

    constructor(protected readonly http: HttpClient,
        protected readonly filterFactory: FilterFactory) {
    }

    getStats(operation: string,
        field: string,
        group1: string = null,
        group2: string = null,
        filters: any = {}): Observable<Stat | SingleSeries | MultiSeries> {
        const url = this.getBaseUrl() + ['stats', group1, group2, operation, field].filter(x => x != null).join('/');
        return this.http.get<Stat>(url, {
            params: this.filterFactory.buildParams(filters)
        }).pipe(
            map(result => {
                if (Array.isArray(result) && result.length > 0) {
                    if (result[0]['series']) {
                        return this.sortMultiSeries(result);
                    } else {
                        return this.sortSingleSeries(result);
                    }
                }
            })
        );
    }

    private sortMultiSeries(ms: MultiSeries): MultiSeries {
        return [...ms.map(ns => ({
            name: ns.name,
            series: this.sortSingleSeries(ns.series)
        }))].sort((a, b) => a.name.localeCompare(b.name));
    }

    private sortSingleSeries(ss: SingleSeries): SingleSeries {
        return [...ss].sort((a, b) => a.name.localeCompare(b.name));
    }

}
