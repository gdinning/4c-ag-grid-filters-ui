import {EmployeeService} from './employee.service';
import {
    IServerSideDatasource,
    IServerSideGetRowsParams
} from '@ag-grid-community/all-modules';
import {Injectable} from '@angular/core';
import {ExoplanetService} from './exoplanet.service';

@Injectable({
    providedIn: 'root'
})
export class ExoplanetDatasource implements IServerSideDatasource {
    rowCount = 0;

    constructor(private readonly exoplanetService: ExoplanetService) {}

    getRows(params: IServerSideGetRowsParams): void {
        this.exoplanetService
            .getExoplanets(params.request.filterModel)
            .subscribe(
                employees =>
                    params.successCallback(employees, employees.length),
                error => params.failCallback()
            );
    }
}
