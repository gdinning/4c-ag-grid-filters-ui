import {ColDef} from '@ag-grid-community/all-modules';
import {Injectable} from '@angular/core';
import {Exoplanet} from 'src/app/model/exoplanet';
import {GridConfig} from '../model/grid-config';
import {ExoplanetService} from './exoplanet.service';
import {ExoplanetDatasource} from './explanet-datasource.service';

@Injectable({
    providedIn: 'root'
})
export class ExoplanetGridConfig extends GridConfig<Exoplanet> {
    constructor(datasource: ExoplanetDatasource, exoplanetService: ExoplanetService) {
        super(datasource);

        const discoveryMethodColumn = ExoplanetGridConfig.columns.find(c => c.field === 'discoveryMethod');
        if (discoveryMethodColumn != null) {
            discoveryMethodColumn.filterParams.items = exoplanetService.getDiscoveryMethods();
        }

        const facilityColumn = ExoplanetGridConfig.columns.find(c => c.field === 'facility');
        if (facilityColumn != null) {
            if (facilityColumn.filterParams == null) {
                facilityColumn.filterParams = {};
            }
            facilityColumn.filterParams.values = (params) => {
                console.log('fetching params?');
                exoplanetService.getFacilities().subscribe(facilities => params.success(facilities));
            };
        }

        this.columns = ExoplanetGridConfig.columns;
    }

    static columns: ColDef[] = [
        {
            headerName: 'Star Name',
            colId: 'starName',
            field: 'star.name',
            filter: 'agTextColumnFilter'
        },
        {
            headerName: 'Planet Count',
            colId: 'planetCount',
            field: 'star.planetCount',
            filter: 'agNumberColumnFilter'
        },
        {
            headerName: 'Planet Name',
            field: 'name',
            filter: 'agTextColumnFilter'
        },
        {
            headerName: 'Discovery Method',
            field: 'discoveryMethod',
            filter: 'selectFilter',
            filterParams: {
                sort: true
            },
            floatingFilterComponent: 'selectFloatingFilter'
        },
        {
            headerName: 'Last Updated',
            field: 'lastUpdated',
            filter: 'agDateColumnFilter',
            filterParams: {
                defaultOption: 'greaterThan'
            }
        },
        {
            headerName: 'Facility',
            field: 'facility',
            filter: 'agSetColumnFilter'
        },
        {
            headerName: 'Telescope',
            field: 'telescope',
            filter: 'agTextColumnFilter'
        },
        {
            headerName: 'Instrument',
            field: 'instrument',
            filter: 'agTextColumnFilter'
        },
        {
            headerName: 'Link',
            field: 'link',
            filter: 'agTextColumnFilter'
        }
    ];
}
