import {Injectable} from '@angular/core';
import {SimpleFilter} from 'src/app/model/simple-filter';
import {CombinedFilter} from 'src/app/model/combined-filter';
import {HttpParams} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class FilterFactory {
    private static readonly unencodedFilters: Set<string> = new Set([]);

    buildParams(filters: any): HttpParams {
        return new HttpParams({fromObject: this.buildFilters(filters)});
    }

    buildFilters(filters: any): any {
        return Object.getOwnPropertyNames(filters).reduce((result, name) => {
            if (FilterFactory.unencodedFilters.has(name) || typeof (filters[name]) === 'string') {
                result[name] = filters[name];
            } else {
                result[name] = this.encodeFilter(
                    this.buildFilter(filters[name])
                );
            }
            return result;
        }, {});
    }

    buildFilter(value: any): CombinedFilter | SimpleFilter {
        return value.operator != null
            ? this.buildCombinedFilter(value)
            : this.buildSimpleFilter(value);
    }

    buildCombinedFilter(value: any): CombinedFilter {
        return {
            predicate: value.operator,
            condition1: this.buildSimpleFilter(value.condition1),
            condition2: this.buildSimpleFilter(value.condition2)
        };
    }

    buildSimpleFilter(value: any): SimpleFilter {
        return {
            predicate: value.type || value.filterType,
            filter: {
                type: value.filterType,
                value: value.filter || value.dateFrom || value.values,
                toValue: value.filterTo || value.dateTo
            }
        };
    }

    encodeFilter(filter: SimpleFilter | CombinedFilter): string {
        return btoa(JSON.stringify(filter));
    }
}
