import {Injectable} from '@angular/core';
import {
    IServerSideDatasource,
    IServerSideGetRowsParams
} from '@ag-grid-community/all-modules';
import {EmployeeService} from './employee.service';

@Injectable({
    providedIn: 'root'
})
export class TestGridDatasource implements IServerSideDatasource {
    constructor(private readonly employeeService: EmployeeService) {}

    getRows(params: IServerSideGetRowsParams): void {
        this.employeeService.getEmployees(params.request.filterModel).subscribe(
            employees => params.successCallback(employees, employees.length),
            error => params.failCallback()
        );
    }
}
