import {Injectable} from '@angular/core';
import {GridConfig} from '../model/grid-config';
import {TestGridDatasource} from './test-grid-datasource.service';
import {ColDef, GridOptions} from '@ag-grid-community/all-modules';

@Injectable({
    providedIn: 'root'
})
export class TestGridConfig extends GridConfig<any> {
    constructor(datasource: TestGridDatasource) {
        super(datasource);
        this.columns = TestGridConfig.columns;
    }

    static columns: ColDef[] = [
        {
            headerName: 'Employee',
            colId: 'employeeName',
            field: 'name',
            filter: 'agTextColumnFilter'
        },
        {
            headerName: 'Sick Days',
            field: 'sickDays',
            filter: 'agNumberColumnFilter'
        }
    ];

}
