import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Exoplanet} from 'src/app/model/exoplanet';
import {Observable} from 'rxjs';
import {FilterFactory} from './filter-factory.service';
import {StatsService} from './stats.service';
import {environment} from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ExoplanetService extends StatsService {

    constructor(http: HttpClient, filterFactory: FilterFactory) {
        super(http, filterFactory);
    }

    getBaseUrl(): string {
        return environment.serviceBaseUrl + '/exoplanet/';
    }

    getExoplanetById(id: number): Observable<Exoplanet> {
        return this.http.get<Exoplanet>(
            `${this.getBaseUrl()}${id}`
        );
    }

    getExoplanets(filters: any): Observable<Exoplanet[]> {
        return this.http.get<Exoplanet[]>(this.getBaseUrl(), {
            params: this.filterFactory.buildParams(filters)
        });
    }

    getDiscoveryMethods(): Observable<string[]> {
        return this.http.get<string[]>(`${this.getBaseUrl()}discoveryMethods`);
    }

    getFacilities(): Observable<string[]> {
        return this.http.get<string[]>(`${this.getBaseUrl()}facilities`);
    }

}
