import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {MatButtonModule, MatTooltipModule} from '@angular/material';
import {AgGridModule} from 'ag-grid-angular';
import {AgGridViewComponent} from './components/ag-grid-view/ag-grid-view.component';
import {SelectFilterModule} from './modules/select-value-filter/select-filter.module';
import {SelectFilterComponent} from './modules/select-value-filter/components/select-filter/select-filter.component';
import {SelectFloatingFilterComponent} from './modules/select-value-filter/components/select-floating-filter/select-floating-filter.component';

@NgModule({
    declarations: [AgGridViewComponent],
    exports: [AgGridViewComponent],
    imports: [
        CommonModule,
        AgGridModule.withComponents([
            SelectFilterComponent,
            SelectFloatingFilterComponent
        ]),
        MatButtonModule,
        HttpClientModule,
        SelectFilterModule
    ]
})
export class AgGridViewModule {}
