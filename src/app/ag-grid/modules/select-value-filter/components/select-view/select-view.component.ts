import {HttpClient} from '@angular/common/http';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, EventEmitter, SimpleChanges, Output, OnChanges} from '@angular/core';
import {SelectFilterItemType} from '../../model/select-filter-params';
import {SelectItem} from '../../model/select-item';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-select-view',
    templateUrl: './select-view.component.html',
    styleUrls: ['./select-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectViewComponent implements OnChanges {

    @Input() clear = false;
    @Input() items: SelectFilterItemType;
    @Input() sort: boolean | ((a: string, b: string) => number);
    @Input() value: string;

    selectItems: SelectItem[];

    @Output() valueChanged = new EventEmitter<string>();

    constructor(protected readonly http: HttpClient, protected readonly cdr: ChangeDetectorRef) {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.items != null) {
            this._updateSelectItems(this.items);
        }
    }

    emit(value: string) {
        if (value !== this.value) {
            this.valueChanged.emit(value);
        }
    }

    private _updateSelectItems(raw: SelectFilterItemType) {
        if (raw == null) {
            this.selectItems = [];
            this.cdr.markForCheck();
        }

        if (Array.isArray(raw)) {
            this.selectItems = this._sort(this._convertToSelectItems(raw));
            this.cdr.markForCheck();

        } else if (typeof (raw) === 'function') {
            this._updateSelectItems(raw() as SelectFilterItemType);

        } else {
            (raw as Observable<string[] | SelectItem[]>).subscribe(r => this._updateSelectItems(r as SelectFilterItemType));
        }
    }

    private _convertToSelectItems(raw: any[]): SelectItem[] {
        if (raw != null && raw.length > 0 && typeof (raw[0]) === 'string') {
            return raw.map(s => ({
                display: s,
                value: s
            }) as SelectItem);

        } else if (raw != null && raw.length > 0 && raw[0].display != null && raw[0].value != null) {
            return raw as SelectItem[];

        } else {
            return [];
        }
    }

    private _sort(items: SelectItem[]): SelectItem[] {
        if (!this.sort) {
            return items;
        } else if (this.sort === true) {
            return items.sort((a: SelectItem, b: SelectItem) => a.display.localeCompare(b.display));
        } else if (typeof (this.sort) === 'function') {
            return items.sort((a: SelectItem, b: SelectItem) => (this.sort as ((a: string, b: string) => number))(a.display, b.display));
        }
    }
}
