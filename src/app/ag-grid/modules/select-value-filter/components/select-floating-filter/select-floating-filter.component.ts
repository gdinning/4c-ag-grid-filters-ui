import {FilterChangedEvent, IFloatingFilter, IFloatingFilterParams, IFilterComp} from '@ag-grid-community/all-modules';
import {AgFrameworkComponent} from '@ag-grid-community/angular';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {SelectFilterParams} from '../../model/select-filter-params';

@Component({
    selector: 'app-select-floating-filter',
    templateUrl: './select-floating-filter.component.html',
    styleUrls: ['./select-floating-filter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectFloatingFilterComponent implements IFloatingFilter, AgFrameworkComponent<IFloatingFilterParams> {

    model: string;
    params: IFloatingFilterParams;
    parentParams: SelectFilterParams;

    private parentChanged = new Subject<string>();

    constructor(private readonly cdr: ChangeDetectorRef) {
    }

    agInit(params: IFloatingFilterParams): void {
        this.params = params;
        this.parentParams = params.filterParams as SelectFilterParams;

        this.parentChanged
            .pipe(debounceTime(100))
            .subscribe(value => {
                if (value !== this.model) {
                    this.model = value;
                    this.cdr.markForCheck();
                }
            });
    }

    onParentModelChanged(parentModel: any, filterChangedEvent?: FilterChangedEvent): void {
        this.parentChanged.next(parentModel);
    }

    updateParent(value: string) {
        this.params.parentFilterInstance((instance: IFilterComp) => {
            instance.setModel(value);
        });
    }
}
