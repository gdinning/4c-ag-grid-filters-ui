import {IDoesFilterPassParams} from '@ag-grid-community/all-modules';
import {IFilterAngularComp} from '@ag-grid-community/angular';
import {Component, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AgFrameworkComponent} from 'ag-grid-angular';
import {SelectFilterParams} from '../../model/select-filter-params';
@Component({
    selector: 'app-select-filter',
    templateUrl: './select-filter.component.html',
    styleUrls: ['./select-filter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectFilterComponent implements IFilterAngularComp, AgFrameworkComponent<SelectFilterParams> {

    model: string;
    params: SelectFilterParams;
    value: string;

    constructor(private readonly cdr: ChangeDetectorRef) {
    }

    agInit(params: SelectFilterParams): void {
        this.params = params;
    }

    isFilterActive(): boolean {
        return !!this.value;
    }

    doesFilterPass(params: IDoesFilterPassParams): boolean {
        return false;
    }

    getModel() {
        return this.model;
    }

    setModel(model: any): void {
        if (model !== this.model) {
            this.model = model;
            this.params.filterChangedCallback();
            this.cdr.markForCheck();
        }
    }

}
