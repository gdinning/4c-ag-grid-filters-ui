import {IFilterParams} from '@ag-grid-community/all-modules';
import {Observable} from 'rxjs';
import {SelectItem} from './select-item';

export type SelectFilterItemType = string[] | SelectItem[] | Observable<string> | Observable<SelectItem[]> |
    (() => Observable<string[]>) | (() => Observable<SelectItem[]>);

export interface SelectFilterParams extends IFilterParams {

    items: SelectFilterItemType;

    sort: boolean | ((a: string, b: string) => number);

}
