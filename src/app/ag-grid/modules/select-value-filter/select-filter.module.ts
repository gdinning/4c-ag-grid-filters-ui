import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatTooltipModule} from '@angular/material';
import {SelectFilterComponent} from './components/select-filter/select-filter.component';
import {SelectFloatingFilterComponent} from './components/select-floating-filter/select-floating-filter.component';
import {SelectViewComponent} from './components/select-view/select-view.component';

@NgModule({
    declarations: [
        SelectFilterComponent,
        SelectFloatingFilterComponent,
        SelectViewComponent
    ],
    exports: [
        SelectFilterComponent,
        SelectFloatingFilterComponent
    ],
    imports: [
        CommonModule,
        MatTooltipModule
    ]
})
export class SelectFilterModule {}
