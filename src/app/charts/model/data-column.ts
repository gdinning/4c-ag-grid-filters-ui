export enum DataColumnType {
    ID, STRING, NUMBER, DATE
}

export enum DataColumnUsage {
    ALL, AGGREGATE, GROUP
}

export class DataColumn {

    constructor(public readonly name: string,
                public readonly display: string,
                public readonly type: DataColumnType,
                public readonly usage = DataColumnUsage.ALL
    ) {
    }

    isNumeric(): boolean {
        return this.type === DataColumnType.NUMBER;
    }

    isString(): boolean {
        return this.type === DataColumnType.STRING;
    }

    isDate(): boolean {
        return this.type === DataColumnType.DATE;
    }

}
