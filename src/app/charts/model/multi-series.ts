import {NamedSeries} from './named-series';

export type MultiSeries = NamedSeries[];
