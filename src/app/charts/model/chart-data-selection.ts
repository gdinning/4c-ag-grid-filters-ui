export interface ChartDataSelection {

    operation: string;
    column: string;
    group1?: string;
    group2?: string;

}
