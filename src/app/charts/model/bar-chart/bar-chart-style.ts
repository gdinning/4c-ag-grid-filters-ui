export enum BarChartStyle {
    SINGLE, GROUPED, NORMALIZED, STACKED
}
