import {DataColumnType} from './data-column';

export class AggregateOperation {

    supportedTypes: Set<DataColumnType>;

    constructor(public readonly name: string,
                public readonly display: string,
                supportedTypesArray: DataColumnType[]) {
        this.supportedTypes = new Set<DataColumnType>(supportedTypesArray);
    }

}

