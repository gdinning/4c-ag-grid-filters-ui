import {DataPoint} from './data-point';

export type SingleSeries = DataPoint[];
