import {InjectionToken} from '@angular/core';
import {DataColumn, DataColumnType} from './data-column';
import {AggregateOperation} from './aggregate-operation';

export const CHART_OPERATIONS = new InjectionToken<AggregateOperation[]>('CHART_OPERATIONS');

export const CHART_COLUMNS = new InjectionToken<DataColumn[]>('CHART_COLUMNS');

export const DEFAULT_COLUMN = new InjectionToken<string>('DEFAULT_COLUMN');

export const DEFAULT_GROUP = new InjectionToken<string>('DEFAULT_GROUP');

export const defaultConfig = {
    scheme: 'vivid',
    schemeType: 'ordinal',
    animations: true,
    xAxis: true,
    yAxis: true,
    showXAxisLabel: true,
    showYAxisLabel: true,
    rotateXAxisTicks: true,
    rotateYAxisTicks: true
};

export const standardOperations = [
    new AggregateOperation('count', 'Count', [DataColumnType.ID]),
    new AggregateOperation('sum', 'Sum', [DataColumnType.NUMBER]),
    new AggregateOperation('avg', 'Average', [DataColumnType.NUMBER]),
    new AggregateOperation('min', 'Min', [DataColumnType.NUMBER, DataColumnType.STRING]),
    new AggregateOperation('max', 'Max', [DataColumnType.NUMBER, DataColumnType.STRING])
];
