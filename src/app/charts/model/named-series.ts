import {SingleSeries} from './single-series';

export interface NamedSeries {

    name: string;
    series: SingleSeries;

}
