import {ChangeDetectionStrategy, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-common-chart-config',
    templateUrl: './common-chart-config.component.html',
    styleUrls: ['./common-chart-config.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommonChartConfigComponent implements OnInit, OnDestroy {

    static readonly defaultData = {
        scheme: 'vivid',
        gradient: false,
        animations: true,
        legend: true,
        legendPosition: 'right'
    };

    readonly schemes = new Map<string, string>([
        ['vivid', 'Vivid'],
        ['natural', 'Natural'],
        ['cool', 'Cool'],
        ['fire', 'Fire'],
        ['solar', 'Solar'],
        ['air', 'Air'],
        ['aqua', 'Aqua'],
        ['flame', 'Flame'],
        ['forest', 'Forest'],
        ['ocean', 'Ocean'],
        ['horizon', 'Horizon'],
        ['neons', 'Neons'],
        ['picnic', 'Picnic'],
        ['night', 'Night'],
        ['nightLights', 'Night Lights']
    ]);

    @Output() configChanged = new EventEmitter<any>();

    form: FormGroup;

    private _destroyed$ = new Subject<void>();

    constructor() {
    }

    ngOnInit() {
        this.form = new FormGroup({
            scheme: new FormControl(CommonChartConfigComponent.defaultData.scheme),
            gradient: new FormControl(CommonChartConfigComponent.defaultData.gradient),
            animations: new FormControl(CommonChartConfigComponent.defaultData.animations),
            legend: new FormControl(CommonChartConfigComponent.defaultData.legend),
            legendPosition: new FormControl(CommonChartConfigComponent.defaultData.legendPosition)
        });

        this.form.valueChanges.pipe(
            takeUntil(this._destroyed$)
        ).subscribe(value => this.configChanged.emit(value));

        this.configChanged.emit(this.form.value);
    }

    ngOnDestroy(): void {
        this._destroyed$.next();
        this._destroyed$.complete();
    }

}
