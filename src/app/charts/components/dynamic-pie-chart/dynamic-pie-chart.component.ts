import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {PieChartType} from '../../model/pie-chart/pie-chart-type';
import {SingleSeries} from '../../model/single-series';
import {defaultConfig} from '../../model/chart-constants';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
    selector: 'app-dynamic-pie-chart',
    templateUrl: './dynamic-pie-chart.component.html',
    styleUrls: ['./dynamic-pie-chart.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicPieChartComponent implements OnInit, OnDestroy {

    PieChartType = PieChartType;

    data: SingleSeries;
    form: FormGroup;

    private _destroyed$ = new Subject<void>();

    constructor(private readonly cdr: ChangeDetectorRef) {
    }

    private _commonConfig: any = defaultConfig;

    set commonConfig(value: any) {
        this._commonConfig = value;
        this.cdr.markForCheck();
    }

    get config(): any {
        return {
            ...this.form.value,
            doughnut: JSON.parse(this.form.value.doughnut || 'false'),
            ...this._commonConfig
        };
    }

    ngOnInit() {
        this.form = new FormGroup({
            type: new FormControl(PieChartType.SIMPLE),
            labels: new FormControl(true),
            doughnut: new FormControl('false')
        });

        this.form.valueChanges
            .pipe(
                takeUntil(this._destroyed$)
            )
            .subscribe(value => this.cdr.markForCheck());
    }

    ngOnDestroy(): void {
        this._destroyed$.next();
        this._destroyed$.complete();
    }

}
