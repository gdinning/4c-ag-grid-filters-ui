import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Inject,
    Input,
    OnDestroy,
    OnInit,
    Optional,
    Output
} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {StatsService} from 'src/app/ag-grid/services/stats.service';
import {Stat} from 'src/app/model/stat';
import {AggregateOperation} from '../../model/aggregate-operation';
import {CHART_COLUMNS, CHART_OPERATIONS, DEFAULT_COLUMN, DEFAULT_GROUP} from '../../model/chart-constants';
import {DataColumn, DataColumnUsage} from '../../model/data-column';
import {MultiSeries} from '../../model/multi-series';
import {SingleSeries} from '../../model/single-series';
import {ChartDataSelection} from '../../model/chart-data-selection';
import {FilterService} from 'src/app/services/filter.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-chart-data-selector',
    templateUrl: './chart-data-selector.component.html',
    styleUrls: ['./chart-data-selector.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartDataSelectorComponent implements OnInit, OnDestroy {

    @Input() showSecondGroup = false;

    @Output() dataChanged = new EventEmitter<Stat | SingleSeries | MultiSeries>();
    @Output() selectionChanged = new EventEmitter<ChartDataSelection>();

    aggregateColumns: DataColumn[];
    columnMap = new Map<string, DataColumn>();
    dataForm: FormGroup;
    defaultColumn: DataColumn;
    defaultGroup: DataColumn;
    groupColumns: DataColumn[];
    isNumericOperation = false;

    private _destroyed$ = new Subject<void>();
    private filters: any = {};

    constructor(private readonly statsService: StatsService,
                @Inject(CHART_OPERATIONS) public readonly operations: AggregateOperation[],
                @Inject(CHART_COLUMNS) public readonly columns: DataColumn[],
                @Inject(DEFAULT_COLUMN) @Optional() private readonly defaultColumnName: string,
                @Inject(DEFAULT_GROUP) @Optional() private readonly defaultGroupName: string,
                private readonly filterService: FilterService
    ) {
    }

    ngOnInit() {
        this.ngOnInitColumns();
        this.ngOnInitForm();
    }

    ngOnDestroy(): void {
        this._destroyed$.next();
        this._destroyed$.complete();
    }

    ngOnInitColumns() {
        this.aggregateColumns = this.columns.filter(c => c.usage === DataColumnUsage.AGGREGATE || c.usage === DataColumnUsage.ALL);
        if (this.defaultColumnName != null) {
            this.defaultColumn = this.aggregateColumns.find(c => c.name === this.defaultColumnName) || this.aggregateColumns[0];
        } else {
            this.defaultColumn = this.aggregateColumns[0];
        }

        this.groupColumns = this.columns.filter(c => c.usage === DataColumnUsage.GROUP || c.usage === DataColumnUsage.ALL);
        if (this.defaultGroupName != null) {
            this.defaultGroup = this.groupColumns.find(c => c.name === this.defaultGroupName) || this.groupColumns[0];
        } else {
            this.defaultGroup = this.groupColumns[0];
        }

        this.columnMap = new Map<string, DataColumn>(this.columns.map(col => [col.name, col]));
    }

    ngOnInitForm() {
        this.dataForm = new FormGroup({
            operation: new FormControl('count'),
            column: new FormControl(this.defaultColumn.name),
            group1: new FormControl(this.defaultGroup.name),
            group2: new FormControl()
        });

        this.dataForm.valueChanges.pipe(
            takeUntil(this._destroyed$)
        ).subscribe(value => {
            this.selectionChanged.emit(value);
            this.load();
        });

        this.dataForm.get('operation').valueChanges.pipe(
            takeUntil(this._destroyed$)
        ).subscribe(() => {
            // Make sure the selected aggregate column is supported by the operation.  If not, select
            // the first aggregate column that is
            const formColumn = this.dataForm.get('column');
            const operation = this.operations.find(o => o.name === this.dataForm.get('operation').value);
            if (!operation.supportedTypes.has(this.aggregateColumns.find(c => c.name === formColumn.value).type)) {
                formColumn.setValue(this.aggregateColumns.find(c => operation.supportedTypes.has(c.type)).name);
            }
        });

        this.selectionChanged.emit(this.dataForm.value);
        this.filterService.getFilters$().pipe(
            takeUntil(this._destroyed$)
        ).subscribe(filters => {
            this.filters = filters;
            this.load();
        });
    }

    isAggregateColumnDisabled(column: DataColumn): boolean {
        const operation = this.operations.find(o => o.name === this.dataForm.get('operation').value);
        return !operation.supportedTypes.has(column.type);
    }

    load() {
        const values = this.dataForm.value;

        const column = this.columnMap.get(values.column) || this.defaultColumn;
        const group1 = this.columnMap.get(values.group1) || this.defaultGroup;
        const group2 = this.columnMap.get(values.group2);

        this.statsService.getStats(values.operation, column.name, group1.name, group2 ? group2.name : null, this.filters).pipe(
            takeUntil(this._destroyed$)
        ).subscribe(data => this.dataChanged.emit(data));
    }

}
