import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {StatsService} from 'src/app/ag-grid/services/stats.service';
import {BarChartDirection} from '../../model/bar-chart/bar-chart-direction';
import {BarChartStyle} from '../../model/bar-chart/bar-chart-style';
import {CHART_COLUMNS, defaultConfig} from '../../model/chart-constants';
import {ChartDataSelection} from '../../model/chart-data-selection';
import {DataColumn} from '../../model/data-column';
import {MultiSeries} from '../../model/multi-series';
import {Stat} from 'src/app/model/stat';
import {SingleSeries} from '../../model/single-series';
import {CommonChartConfigComponent} from '../common-chart-config/common-chart-config.component';

@Component({
    selector: 'app-dynamic-bar-chart',
    templateUrl: './dynamic-bar-chart.component.html',
    styleUrls: ['./dynamic-bar-chart.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicBarChartComponent implements OnInit {

    BarChartDirection = BarChartDirection;
    BarChartStyle = BarChartStyle;

    chartConfig: any;
    chartForm: FormGroup;
    columnMap = new Map<string, DataColumn>();
    data: Stat | SingleSeries | MultiSeries;
    isMultiSeries: boolean;

    constructor(private readonly statsService: StatsService,
                @Inject(CHART_COLUMNS) columns: DataColumn[],
                public readonly cdr: ChangeDetectorRef) {

        columns.forEach(col => this.columnMap.set(col.name, col));
    }

    ngOnInit() {

        this.chartForm = new FormGroup({
            direction: new FormControl(BarChartDirection.VERTICAL),
            style: new FormControl(BarChartStyle.SINGLE)
        });

        this.chartConfig = {
            ...defaultConfig,
            ...CommonChartConfigComponent.defaultData
        };

    }

    configChanged(config: any) {
        this.chartConfig = {
            ...this.chartConfig,
            ...config
        };
        this.cdr.markForCheck();
    }

    dataChanged(data: Stat | SingleSeries | MultiSeries) {
        this.data = data;
        const isMultiSeries = this.data && this.data['length'] > 0 && this.data[0]['series'] != null;
        const styleControl = this.chartForm.get('style');
        if (isMultiSeries && !this.isMultiSeries) {
            styleControl.setValue(BarChartStyle.STACKED);
        } else if (!isMultiSeries && this.isMultiSeries) {
            this.chartForm.get('style').setValue(BarChartStyle.SINGLE);
        }
        this.isMultiSeries = isMultiSeries;
        window.dispatchEvent(new Event('resize'));
    }

    dataSelectionChanged(selection: ChartDataSelection) {
        this.setAxisLabels(selection);
    }

    setAxisLabels(selection: ChartDataSelection) {
        this.chartConfig.yAxisLabel = `${selection.operation} ( ${this.columnMap.get(selection.column).display} )`;
        this.chartConfig.xAxisLabel = selection.group2 ?
            [this.columnMap.get(selection.group1).display, this.columnMap.get(selection.group2).display].join(' / ') :
            this.columnMap.get(selection.group1).display;
    }

}
