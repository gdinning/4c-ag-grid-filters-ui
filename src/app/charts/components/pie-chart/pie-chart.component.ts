import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {PieChartType} from '../../model/pie-chart/pie-chart-type';
import {SingleSeries} from '../../model/single-series';
import {MultiSeries} from '../../model/multi-series';
import {defaultConfig} from '../../model/chart-constants';

@Component({
    selector: 'app-pie-chart',
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PieChartComponent {

    PieChartType = PieChartType;

    @Input() config: any = defaultConfig;
    @Input() data: SingleSeries | MultiSeries;
    @Input() type = PieChartType.SIMPLE;

}
