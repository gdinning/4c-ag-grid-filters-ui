import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnChanges,
    OnInit,
    SimpleChanges
} from '@angular/core';
import {BarChartDirection} from '../../model/bar-chart/bar-chart-direction';
import {BarChartStyle} from '../../model/bar-chart/bar-chart-style';
import {BarChartType} from '../../model/bar-chart/bar-chart-type';
import {multi} from './data';
import {SingleSeries} from '../../model/single-series';
import {MultiSeries} from '../../model/multi-series';
import {defaultConfig} from '../../model/chart-constants';

@Component({
    selector: 'app-bar-chart',
    templateUrl: './bar-chart.component.html',
    styleUrls: ['./bar-chart.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BarChartComponent implements OnInit, OnChanges {

    private static readonly defaultDirection = BarChartDirection.VERTICAL;
    private static readonly defaultStyle = BarChartStyle.GROUPED;
    private static readonly types = {
        [BarChartStyle.SINGLE]: {
            [BarChartDirection.HORIZONTAL]: BarChartType.HORIZONTAL_BAR_CHART,
            [BarChartDirection.VERTICAL]: BarChartType.VERTICAL_BAR_CHART
        },
        [BarChartStyle.GROUPED]: {
            [BarChartDirection.HORIZONTAL]: BarChartType.HORIZONTAL_GROUPED_BAR_CHART,
            [BarChartDirection.VERTICAL]: BarChartType.VERTICAL_GROUPED_BAR_CHART
        },
        [BarChartStyle.NORMALIZED]: {
            [BarChartDirection.HORIZONTAL]: BarChartType.HORIZONTAL_NORMALIZED_BAR_CHART,
            [BarChartDirection.VERTICAL]: BarChartType.VERTICAL_NORMALIZED_BAR_CHART
        },
        [BarChartStyle.STACKED]: {
            [BarChartDirection.HORIZONTAL]: BarChartType.HORIZONTAL_STACKED_BAR_CHART,
            [BarChartDirection.VERTICAL]: BarChartType.VERTICAL_STACKED_BAR_CHART
        }
    };

    BarChartType = BarChartType;

    @Input() config: any = defaultConfig;
    @Input() data: SingleSeries | MultiSeries;
    @Input() direction = BarChartComponent.defaultDirection;
    @Input() style = BarChartComponent.defaultStyle;

    public type: BarChartType;

    constructor(private readonly cdr: ChangeDetectorRef) {
        Object.assign(this, {multi});
    }

    ngOnInit() {
        this.calculateChartType();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.direction != null || changes.style != null || changes.data != null) {
            this.calculateChartType();
        }
    }

    private calculateChartType(): void {
        const style = this.style != null ? this.style : BarChartComponent.defaultStyle;
        const direction = this.direction != null ? this.direction : BarChartComponent.defaultDirection;
        this.type = BarChartComponent.types[style][direction];
        this.cdr.markForCheck();
    }

}
