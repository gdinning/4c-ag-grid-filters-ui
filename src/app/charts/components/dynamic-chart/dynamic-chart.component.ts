import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ChartType} from '../../model/chart-type';

@Component({
    selector: 'app-dynamic-chart',
    templateUrl: './dynamic-chart.component.html',
    styleUrls: ['./dynamic-chart.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicChartComponent {

    ChartType = ChartType;

    type = ChartType.BAR;

}
