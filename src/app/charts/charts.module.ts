import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BarChartComponent} from './components/bar-chart/bar-chart.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PieChartComponent} from './components/pie-chart/pie-chart.component';
import {DynamicPieChartComponent} from './components/dynamic-pie-chart/dynamic-pie-chart.component';
import {ChartDataSelectorComponent} from './components/chart-data-selector/chart-data-selector.component';
import {CommonChartConfigComponent} from './components/common-chart-config/common-chart-config.component';
import {DynamicChartComponent} from './components/dynamic-chart/dynamic-chart.component';
import {DynamicBarChartComponent} from './components/dynamic-bar-chart/dynamic-bar-chart.component';

@NgModule({
    declarations: [
        BarChartComponent,
        DynamicBarChartComponent,
        DynamicChartComponent,
        PieChartComponent,
        DynamicPieChartComponent,
        ChartDataSelectorComponent,
        CommonChartConfigComponent
    ],
    exports: [
        DynamicChartComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        CommonModule,
        FormsModule,
        NgxChartsModule,
        ReactiveFormsModule
    ]
})
export class ChartsModule {
}
