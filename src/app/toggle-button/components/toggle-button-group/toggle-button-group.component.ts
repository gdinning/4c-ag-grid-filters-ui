import {Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, AfterViewInit} from '@angular/core';
import {ToggleButtonGroupService} from '../../service/toggle-button-group.service';

@Component({
    selector: 'app-toggle-button-group',
    templateUrl: './toggle-button-group.component.html',
    styleUrls: ['./toggle-button-group.component.scss'],
    providers: [
        ToggleButtonGroupService
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToggleButtonGroupComponent implements OnInit {

    @Input() initialValue: string;
    @Output() valueChanged = new EventEmitter<string>();

    value: string;

    constructor(private readonly service: ToggleButtonGroupService) {
        service.getValue$().subscribe(value => {
            this.value = value;
            this.valueChanged.emit(value);
        });
    }

    ngOnInit() {
        if (this.initialValue != null) {
            this.service.setValue(this.initialValue);
        }
    }

}
