import {Component, OnInit, ChangeDetectionStrategy, Input, HostBinding, ElementRef, ChangeDetectorRef} from '@angular/core';
import {ToggleButtonGroupService} from '../../service/toggle-button-group.service';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-toggle-button',
    templateUrl: './toggle-button.component.html',
    styleUrls: ['./toggle-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToggleButtonComponent implements OnInit {

    @Input() default = false;
    @Input() value: string;

    @HostBinding('class') hostClasses = 'level-item';

    classes: string;
    selection: string;
    value$: Observable<string>;

    constructor(public readonly service: ToggleButtonGroupService, el: ElementRef, cdr: ChangeDetectorRef) {

        this.classes = el.nativeElement.getAttribute('class');

        service.getValue$().subscribe(selection => {
            this.selection = selection;
            cdr.markForCheck();
        });

        this.value$ = service.getValue$();
    }

    ngOnInit() {
        if (this.default) {
            this.service.setValue(this.value);
        }
    }

}
