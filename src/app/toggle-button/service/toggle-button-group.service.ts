import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {share, startWith} from 'rxjs/operators';

@Injectable()
export class ToggleButtonGroupService {

    value: string;

    // tslint:disable-next-line: variable-name
    private _value$ = new Subject<string>();

    getValue$() {
        return this._value$.pipe(
            share(),
            startWith(this.value)
        );
    }

    setValue(value: string) {
        this.value = value;
        this._value$.next(value);
    }

}
