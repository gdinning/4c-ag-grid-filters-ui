import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToggleButtonComponent} from './components/toggle-button/toggle-button.component';
import {ToggleButtonGroupComponent} from './components/toggle-button-group/toggle-button-group.component';

@NgModule({
    declarations: [
        ToggleButtonComponent,
        ToggleButtonGroupComponent
    ],
    exports: [
        ToggleButtonComponent,
        ToggleButtonGroupComponent
    ],
    imports: [
        CommonModule
    ]
})
export class ToggleButtonModule {}
