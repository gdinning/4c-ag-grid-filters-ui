import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SplitService {

    getSplit(key: string): number[] {
        const value = localStorage.getItem(key);
        return !!value ? JSON.parse(value) : [50, 50];
    }

    setSplit(key: string, sizes: number[]) {
        localStorage.setItem(key, JSON.stringify(sizes));
        window.dispatchEvent(new Event('resize'));
    }

}
