import {Injectable} from '@angular/core';
import {FilterFactory} from '../ag-grid/services/filter-factory.service';
import {Subject, Observable} from 'rxjs';
import {share, startWith} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class FilterService {

    private filters: any = {};
    private filters$ = new Subject<any>();

    constructor(private readonly filterFactory: FilterFactory) {
    }

    getFilters(): any {
        return this.filters;
    }

    getFilters$(): Observable<any> {
        return this.filters$.pipe(
            share(),
            startWith(this.filters),
        );
    }

    setFilters(value: any) {
        this.filters = this.filterFactory.buildFilters(value);
        this.filters$.next(this.filters);
    }

}
