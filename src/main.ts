import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

import {LicenseManager} from '@ag-grid-enterprise/core';

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));

// tslint:disable-next-line:max-line-length
LicenseManager.setLicenseKey('CompanyName=Communication Security Establishment,LicensedApplication=CONCERT,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=50,LicensedProductionInstancesCount=0,AssetReference=AG-007649,ExpiryDate=3_April_2021_[v2]_MTYxNzQwODAwMDAwMA==8eb7ad5400c57b3e3fd7f2de2487c17d');
